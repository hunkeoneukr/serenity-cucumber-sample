# Cucumber Starter
Test Project is using Java 8 and Serenity-BDD.

##IMPORTANT!
I haven't found a quick solution to copy sources from main to test during compile so the fastest solution was to add 2 profiles:
1 for test runs and 1 to do a release (so the step library will be available as dependency)

## Project Structure
```
src
  + main
    + java                        Step definition implementations and supporting code
  + test
    + java                        Test runners
    + resources
      + features                  Feature files
      + search                    Feature file subdirectories
             search_by_keyword.feature
```

## Tests execution and reporting
To execute tests run:
```shell
mvn clean insall
```
After process is finished report will be available in
```shell
~${projectDir}/target/site/serenity
```
Report home page is default _index.html_

#### Using docker
To run the tests using docker excecute the following:
```shell
docker-compose up
```
Report will be present in _report_ directory at a project root.

#### IMPORTANT!
Please note, I'll be able to verify docker setup next week.
New pc is on route and Win home doesn't support docker :(

## Adding new test
Tests are using gherkin script (_Given/When/Then_).

First add new feature or scenario with corresponding steps. 

Please keep implementation of the step definitions in main.
This will allow to re-use already existing step definitions in other projects.

As the second step create step definition implementation

## Releasing step library
To release step library use corresponding profile and perform release using 
[maven-release-plugin](https://maven.apache.org/maven-release/maven-release-plugin/examples/prepare-release.html).
example:
```shell
mvn release:prepare
mvn release:perform
```
This would create a corresponding git tag and will upload step library to artifactory
