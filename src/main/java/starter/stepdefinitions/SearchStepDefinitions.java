package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class SearchStepDefinitions extends BaseStepDefinition {

    private static final String SEARCH_CONTEXT = "/api/v1/search/test/";

    @When("he searches for {string}")
    public void heCallsEndpoint(String item) {
        SerenityRest.given().log().all().get(getBaseUrl() + SEARCH_CONTEXT + item);
    }

    @Then("the error is shown")
    public void theErrorIsShown() {
        SerenityRest.restAssuredThat(response -> response.statusCode(404));
        SerenityRest.restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }

    @Then("he sees the results displayed {string}")
    public void heSeesTheResultsDisplayedFor(String item) {
        SerenityRest.restAssuredThat(response -> response.statusCode(200));
        Response r = SerenityRest.lastResponse();
        String actual = r.then().extract().path("title").toString();
        assertThat(actual, containsString(item));
    }

}
