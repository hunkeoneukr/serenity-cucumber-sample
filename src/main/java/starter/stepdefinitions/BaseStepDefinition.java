package starter.stepdefinitions;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

public class BaseStepDefinition {

    private EnvironmentVariables environmentVariables;

    public String getBaseUrl() {
        return EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("fruit.restapi.baseurl");
    }


}
