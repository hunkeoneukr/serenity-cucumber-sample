package starter.flowdefinitions;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.restassured.response.Response;
import net.minidev.json.JSONArray;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.stepdefinitions.SearchStepDefinitions;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SearchFlowStepDefinitions {

    private static final String TITLE_KEY = "title";
    private static final String BRAND_KEY = "brand";
    private static final String PROVIDER_KEY = "provider";

    @Steps
    private SearchStepDefinitions searchSteps;

    // Just in case we'll have another level of abstraction and will
    // have a need to access lower level of definitions
    public SearchStepDefinitions getSearchSteps() {
        return this.searchSteps;
    }

    @Given("^working fruits api$")
    public void workingFruitsApi(DataTable table) {
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> columns : rows) {
            // Creating new map to pass verification without Type column data as source is immutable
            Map<String, String> cols = new HashMap<>(columns);
            cols.remove("Type");
            searchSteps.heCallsEndpoint(columns.get("Type"));
            Response response = SerenityRest.lastResponse();
            JSONArray matchingFruits = JsonPath.parse(response.getBody().asString()).read("$.[?(@.title=='" + columns.get(TITLE_KEY) + "')]");
            boolean isMatchingResultFound = isMatchingResult(cols, matchingFruits);
            assertTrue(isMatchingResultFound, "No matching result is found for: title: " + columns.get(TITLE_KEY));
        }
    }

    private boolean isMatchingResult(Map<String, String> columns, JSONArray responseFilteredResult) {
        boolean isMatchingResultFound = false;
        Set<String> columnTitles = columns.keySet();
        // Going through response object
        for (Object value : responseFilteredResult) {
            LinkedHashMap o = (LinkedHashMap) value;
            boolean isMatchingColumnValue = false;
            // Comparing datatable to response value.
            // response object considered not equal if at least 1 field doesn't match
            for (String t : columnTitles) {
                if (o.get(t).equals(columns.get(t))) {
                    isMatchingColumnValue = true;
                } else {
                    isMatchingColumnValue = false;
                    break;
                }
            }
            isMatchingResultFound = isMatchingColumnValue;
            // If matching object is present end iteration. No need to check other response objects
            if (isMatchingResultFound) {
                break;
            }
        }
        return isMatchingResultFound;
    }
}
