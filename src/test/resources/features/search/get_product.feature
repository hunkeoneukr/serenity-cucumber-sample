Feature: Simple Product search steps

  Scenario: Search for an apple
    When he searches for "apple"
    Then he sees the results displayed 'apple'

  Scenario: Search for a mango
    When he searches for "mango"
    Then he sees the results displayed 'mango'

  Scenario: Verify absence of the search result
    When he searches for "car"
    Then the error is shown
