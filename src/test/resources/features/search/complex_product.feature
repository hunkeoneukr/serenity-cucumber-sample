Feature: Complex product search step

  Scenario: Datatable example with inject of other step definitions
    Given working fruits api
      | Type  | title                                         | provider | brand        |
      | apple | Apple Bandit Cider Juicy Apple Fles 6 x 30 cl | Vomar    | Apple Bandit |
      | mango | Fuze Tea Green Tea Mango Chamomile 1,5 L      | Vomar    | Fuze Tea     |
